#!/bin/sh
set -x
FOLDER="$(dirname "$0")"

[ ! -e "$FOLDER/ecommerce.db" ] && "$FOLDER/setup-db.sh"
[ ! -e "$FOLDER/venv" ] && "$FOLDER/setup-env.sh"

. "$FOLDER/venv/bin/activate" 2> /dev/null
cd "$FOLDER"
exec py.test -v "$@" ./tests
