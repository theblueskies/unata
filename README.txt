This repository is a coding test for qualified
candidates to demonstrate their ability to manipulate
and create new api's within this Flask / SQLAlchemy
project.


Setup Project
-------------

The setup requires the following software to be
installed on the participants machine:

* sqlite3
* Python 2 or Python 3
* Virtualenv (if using Python 2)

All that is required to setup the environment and run
the api is executing the following...

    $ ./run.sh

The above will create an `ecommerce.db` file (if one is
missing) and create the `./venv` virtual environment
folder if this is missing as well.  Removing either
file or folder is a good way to quickly reset certain
pieces of the project if you run into trouble.


Running Tests
-------------

Tests are written using `pytest`.  To run tests
automatically, it's sufficient to execute the
following...

    $ ./tests.sh

However, if greater control is required, it should be
suffient to ensure the virtualenv has been setup,
activated, and the command `py.test` executed within
the root of the project.

    $ cd <root of unatainc project>
    $ ./setup-env.sh
    $ . ./venv/bin/activate
    $ py.test

Tests for the `/orders` and `/cart` end-points should
be written by the applicant.

Resetting the database fixtures to run tests should be
sufficent to restore testing environment if things
should go astray.

    $ ./setup-db.sh

Currently, tests are dependant on having data available
in the database, but tests are no dependant on specific
content within the database.  Data retrieved from end-
points are cross-compared with data found in the
database.


Current API
-----------

The current API only has the `/users` and `/products`
endpoints setup.  There is no authentication or session
information managed by the api.

The project is intended to run as a single process
since user information (ie. the user id) is managed in
memory and made available globally within the
framework.

The curl requests outlined below is partially
implemented using this project.  Whatever is incomplete
should be developed within the api.

Keep in mind the following implementation details when
creating the new api.  Also look at the implementation
for `/users` and `/products` to help fast track the
development of the `/cart` and `/orders` endpoints.

    * There can be many orders per user, but only one
      cart per user.  Switching users should reveal a
      different cart.

    * The cart should be created on first use by a user.

    * The cart should be emptied when posted to create
      an order.

    * Orders which are fetched by id and does not
      pertain to the current user should not be
      retrieved.


# Get current user.
curl -X GET http://localhost:5000/user

Payload:
    {"id": 1, name: "Elliot Alderson"}


# Set current user.
curl -X PUT \
    -H 'Content-Type: application/json' \
    --data '{"id": 5}' \
    http://localhost:5000/user

    Payload:
        {"id": 2, name: "Angela Moss"}


# Get a list of products
curl -X GET http://localhost:5000/products

    Payload:
        {
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 2,
              "name": "Bananas"
            },
            {
              "id": 3,
              "name": "Oranges"
            }
        }


# Get a single product.
curl -X GET http://localhost:5000/products/1

    Payload:
        {
          "id": 1,
          "name": "Coffee"
        }


# Get the current cart.
curl -X GET http://localhost:5000/cart

    Payload:
        {
          "id": 5,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 2,
              "name": "Bananas"
            }
          ]
        }


# Put products into the cart.
curl -X PUT \
    -H 'Content-Type: application/json' \
    --data '{"products": [{"id": 3}, {"id": 4}]}' \
    http://localhost:5000/cart

    Payload:
        {
          "id": 5,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 2,
              "name": "Bananas"
            },
            {
              "id": 3,
              "name": "Oranges"
            },
            {
              "id": 4,
              "name": "Pears"
            }
          ]
        }


# Set specific products in the cart (reseting other products)
# This is a good way to empty the cart as well.
curl -X POST \
    -H 'Content-Type: application/json' \
    --data '{"products": [{"id": 1}, {"id": 3}]}' \
    http://localhost:5000/cart

    Payload:
        {
          "id": 5,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 3,
              "name": "Oranges"
            }
          ]
        }


# Delete the current cart.
curl -X DELETE http://localhost:5000/cart


# Convert the cart into an order.
curl -X POST --data '' http://localhost:5000/cart/order

    Payload:
        {
          "id": 5,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 3,
              "name": "Oranges"
            }
          ]
        }


# Get details on a single order.
curl -X GET http://localhost:5000/orders/1

    Payload:
        {
          "id": 1,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 3,
              "name": "Oranges"
            }
          ]
        }


# Fetch all orders
curl -X GET http://localhost:5000/orders

    Payload:
    {
      "orders": [
        {
          "id": 5,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 3,
              "name": "Oranges"
            }
          ]
        },
        {
          "id": 6,
          "products": [
            {
              "id": 1,
              "name": "Coffee"
            },
            {
              "id": 2,
              "name": "Bananas"
            }
          ]
        }
      ]
    }
