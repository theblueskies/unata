#!/bin/sh
set -x
FOLDER="$(dirname "$0")"

[ -e "$FOLDER/venv" ] && rm -rf "$FOLDER/venv"
command -v virtualenv && virtualenv "$FOLDER/venv" || python -m venv "$FOLDER/venv"
. "$FOLDER/venv/bin/activate"
pip install --upgrade pip
pip install -r requirements.txt
