from flask import g

from unatainc import database as db


def get_user():
    return (
        g.session.query(db.User)
        .filter_by(id=g.user.id)
        .first()
    )


def get_user_cart(user):
    return (
        g.session.query(db.Cart)
        .filter(db.Cart.user_id==int(user.id))
        .first()
    )


def get_cart_products(cart):
    return (
        g.session.query(db.CartProduct, db.Product)
        .filter(db.CartProduct.product_id==db.Product.id)
        .filter(db.CartProduct.cart_id==cart.id)
    )


def get_validated_products(product_ids):
    return (
        g.session.query(db.Product)
        .filter(db.Product.id.in_(product_ids))
        .all()
    )


def get_or_create_cart(user):
    cart = get_user_cart(user)

    if not cart:
        cart = db.Cart(user_id=user.id)
        g.session.add(cart)
        g.session.commit()

    return cart
