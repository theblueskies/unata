from flask import g, Blueprint

from unatainc import database as db
from unatainc.utils import to_json, from_json
from unatainc.controllers.utils_db import get_user, get_user_cart, \
    get_cart_products, get_validated_products, get_or_create_cart


bp = Blueprint('cart', __name__, url_prefix='/cart')


@bp.route('/')
def get_cart():
    """Retrieves the contents of a cart and returns cart id and list of all products in it
    """
    user = get_user()
    cart = get_user_cart(user)

    if not cart:
        return to_json({'error': 'Cart not found for user {}'.format(user.id)}), 404

    cart_product_details = []
    for cart_product, product in get_cart_products(cart):
        cart_product_details.append({'id': product.id, 'name': product.name})

    return to_json(dict(id=cart.id, products=cart_product_details)), 200


@bp.route('/', methods=['PUT'])
def put_cart():
    """Updates the contents of the cart

    Updates the contents of the cart. It validates any supplied Product Ids, and then
    adds the validated products to the cart.
    """
    user = get_user()

    requested_product_ids = [item['id'] for item in g.params.get('products')]

    if requested_product_ids == []:
        return to_json({'error': 'No product ids supplied. Perhaps you want to use POST'}), 400

    # Validate if the supplied Product IDs exist
    if requested_product_ids != []:
        validated_products = get_validated_products(requested_product_ids)
        if not validated_products:
            return to_json({'error': 'None of the products in the request exist in the db. Sorry!'}), 404

    # Get or create cart
    cart = get_or_create_cart(user)

    # Collect any existing items in cart
    cart_product_details = []
    for cart_product, product in get_cart_products(cart):
        cart_product_details.append({'id': product.id, 'name': product.name})

    # Store validated products in cart
    cart_product_objects = []
    for product in validated_products:
        cart_product_objects.append(db.CartProduct(cart_id=cart.id, product_id=product.id))
        cart_product_details.append({'id': product.id, 'name': product.name})

    g.session.bulk_save_objects(cart_product_objects, return_defaults=True)
    g.session.commit()

    response = {'id': cart.id, 'products': cart_product_details}
    return to_json(response), 201


@bp.route('/', methods=['POST'])
def post_cart():
    """Creates, replaces, empties contents of cart

    This endpoint creates, replaces, empties contents of cart, while keeping the
    cart id the same.

    If the POST body is an empty array, then the cart is emptied.
    """
    user = get_user()

    requested_product_ids = [item['id'] for item in g.params.get('products')]
    validated_products = []
    # Validate if the supplied Product IDs exist
    if requested_product_ids != []:
        validated_products = get_validated_products(requested_product_ids)
        if not validated_products:
            return to_json({'error': 'None of the products in the request exist in the db. Sorry!'}), 404

    # Try to get existing cart of user
    cart = get_or_create_cart(user)

    # Remove existing products from cart
    for cart_product, product in get_cart_products(cart):
        g.session.delete(cart_product)

    # Add validated products to cart
    cart_product_details = []
    cart_product_objects = []
    for product in validated_products:
        cart_product_objects.append(db.CartProduct(cart_id=cart.id, product_id=product.id))
        cart_product_details.append({'id': product.id, 'name': product.name})

    g.session.bulk_save_objects(cart_product_objects, return_defaults=True)
    g.session.commit()
    response = {'id': cart.id, 'products': cart_product_details}
    return to_json(response), 201


@bp.route('/', methods=['DELETE'])
def delete_cart():
    """Deletes a cart

    Deletes a cart. This also deletes any instances of CartProduct that may
    have been related to this cart. This is achieved by a cascading delete
    placed directly on the model definition.
    """
    user = get_user()
    cart = get_user_cart(user)

    if cart:
        g.session.delete(cart)
        g.session.commit()

    return '', 204


@bp.route('/order', methods=['POST'])
def create_order_from_cart():
    """Creates order from cart

    If there are any products supplied in the POST body, they are added to
    the final order, together with any other products that may have been
    added to the user's cart prior.

    If the user does not have an existing cart, a cart is created for him,
    the products added, and the order placed.
    """
    user = get_user()

    requested_product_ids = [item['id'] for item in g.params.get('products')]
    validated_products = []
    # Validate if the supplied Product IDs exist
    if requested_product_ids != []:
        validated_products = get_validated_products(requested_product_ids)
        if not validated_products:
            return to_json({'error': 'None of the products in the request exist in the db. Sorry!'}), 404

    # Try to get existing cart of user
    cart = get_or_create_cart(user)

    # Add validated products to cart
    cart_product_objects = []
    for product in validated_products:
        cart_product_objects.append(db.CartProduct(cart_id=cart.id, product_id=product.id))

    # Create order, and commit CartProducts and Order
    g.session.bulk_save_objects(cart_product_objects, return_defaults=True)
    order = db.Order(user_id=user.id)
    g.session.add(order)
    g.session.commit()

    order_product_list = []
    order_product_objects = []
    # Create OrderProducts
    for cart_product, product in get_cart_products(cart):
        order_product_objects.append(db.OrderProduct(order_id=order.id, product_id=product.id))
        order_product_list.append({'id': product.id, 'name': product.name})

    g.session.bulk_save_objects(order_product_objects, return_defaults=True)
    g.session.delete(cart)
    g.session.commit()

    response = {'order_id': cart.id, 'products': order_product_list}
    return to_json(response), 201
