from flask import g, Blueprint

from unatainc import database as db
from unatainc.utils import to_json, from_json

bp = Blueprint('orders', __name__, url_prefix='/orders')


@bp.route('/<int:order_id>')
def get_order(order_id):
    """Get a specific order and all products in that order"""
    order_and_products = (
        g.session.query(db.OrderProduct, db.Product)
        .filter(db.OrderProduct.order_id==order_id)
        .filter(db.OrderProduct.id==db.Product.id)
    )

    product_list = []
    for order, product in order_and_products:
        product_list.append({'id': product.id, 'name': product.name})

    return to_json({'id': order_id, 'products': product_list}), 200


@bp.route('/')
def get_all_orders():
    """Get all the data in one query and then do operations in memory"""
    orders_and_products = (
        g.session.query(db.Order, db.OrderProduct, db.Product)
        .filter(db.Order.user_id==g.user.id)
        .filter(db.OrderProduct.order_id==db.Order.id)
        .filter(db.OrderProduct.product_id==db.Product.id)
        .order_by(db.Order.id)
    )
    curr_order_id = orders_and_products[0][0].id
    orders = []
    curr_products = []

    for order, order_product, product in orders_and_products:
        if order.id == curr_order_id:
            curr_products.append({'id': product.id, 'name': product.name})
        else:
            orders.append({'id': curr_order_id, 'products': curr_products})
            curr_order_id = order.id
            curr_products = [{'id': product.id, 'name': product.name}]

    # Append the last order set.
    orders.append({'id': curr_order_id, 'products': curr_products})
    return to_json({'orders': orders}), 200
