#!/bin/sh
set -x
FOLDER="$(dirname "$0")"
[ -e "$FOLDER/ecommerce.db" ] && rm "$FOLDER/ecommerce.db"
cat "$FOLDER/ecommerce.sql" | sqlite3 "$FOLDER/ecommerce.db"
