import unatainc.database as db
from unatainc.utils import from_json, to_json


def setup_data(app, session):
    user = (
        session
        .query(db.User)
        .first()
    )

    products = (
        session
        .query(db.Product)
        .order_by(db.Product.id)
        .all()
    )

    order = db.Order(user_id=user.id)
    session.add(order)
    session.commit()

    for product in products:
        order_product = db.OrderProduct(order_id=order.id, product_id=product.id)
        session.add(order_product)
    session.commit()

    return user, order, products


def test_get_single_order(app, session):
    user, order, products = setup_data(app, session)
    expected_products = [{'id': product.id, 'name': product.name} for
                         product in products]

    response = app.get('/orders/1')
    assert response.status_code == 200
    assert from_json(response.data)['products'] == expected_products

def test_get_all_orders(app, session):
    user, order, products = setup_data(app, session)
    products_list = [{'id': product.id, 'name': product.name} for product in products]
    order2 = db.Order(user_id=user.id)
    session.add(order2)
    session.commit()

    order2_product = db.OrderProduct(order_id=order2.id, product_id=products[0].id)
    session.add(order2_product)
    session.commit()

    expected_response = {
        'orders': [
            {
                'id': order.id,
                'products': products_list
            },
            {
                'id': order2.id,
                'products': [{'id': products[0].id, 'name': products[0].name}]
            }
        ]
    }

    response = app.get('/orders')
    assert response.status_code == 200
    assert from_json(response.data) == expected_response
