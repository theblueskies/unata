import pytest

from unatainc import create_app
from unatainc.database import create_engine, create_session


@pytest.fixture(scope='function')
def session(request):
    engine = create_engine()

    connection = engine.connect()
    transaction = connection.begin()

    session = create_session(connection)

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope='function')
def app(session, request):
    app = create_app(__name__, session)

    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app.test_client()
