import unatainc.database as db
from unatainc.utils import from_json


def test_get_products(app, session):
    response = app.get('/products')
    payload = from_json(response.get_data())

    products = payload.get('products')
    assert len(products) > 0

    # test up to three products
    for product in products[:3]:
        assert 'name' in product and product['name']
        assert 'id' in product and product['id']

        product_model = (
            session
            .query(db.Product)
            .filter_by(id=product['id'])
            .first()
        )

        assert product['id'] == product_model.id
        assert product['name'] == product_model.name


def test_get_product(app, session):
    response = app.get('/products')
    payload = from_json(response.get_data())

    products = payload.get('products')
    assert len(products) > 0

    # test up to three products
    for product in products[:3]:
        response = app.get('/products/{}'.format(product['id']))
        payload = from_json(response.get_data())
        assert product == payload

        product_model = (
            session
            .query(db.Product)
            .filter_by(id=payload['id'])
            .first()
        )

        assert payload['id'] == product_model.id
        assert payload['name'] == product_model.name
