import unatainc.database as db
from unatainc.utils import from_json, to_json


def test_get_user(app, session):
    response = app.get('/user')
    payload = from_json(response.get_data())

    assert 'id' in payload and payload['id']
    assert 'name' in payload and payload['name']

    user = (
        session
        .query(db.User)
        .filter_by(id=payload['id'])
        .first()
    )

    assert payload['id'] == user.id
    assert payload['name'] == user.name


def test_set_user(app, session):
    response = app.get('/user')
    payload = from_json(response.get_data())

    assert payload
    current_user_id = payload['id']

    # fetch user that is not the current user.
    user = (
        session.query(db.User)
        .filter(~db.User.id.in_([current_user_id]))
        .limit(1)
        .first()
    )

    # change user within api and test response.
    headers = {'content-type': 'application/json'}
    payload = dict(id=user.id)
    response = app.put('/user', data=to_json(payload), headers=headers)

    payload = from_json(response.get_data())
    assert payload['id'] == user.id
    assert payload['name'] == user.name

    # test fetch user via api is still the new user.
    response = app.get('/user')
    payload = from_json(response.get_data())

    assert payload['id'] == user.id
    assert payload['name'] == user.name
