import unatainc.database as db
from unatainc.utils import from_json, to_json


def setup_data(app, session):
    # Setup db data for tests
    user = (
        session
        .query(db.User)
        .first()
    )

    products = (
        session
        .query(db.Product)
        .order_by(db.Product.id)
        .all()
    )

    cart = db.Cart(user_id=user.id)
    session.add(cart)
    session.commit()

    cart_product_1 = db.CartProduct(cart_id=cart.id, product_id=products[0].id)
    cart_product_2 = db.CartProduct(cart_id=cart.id, product_id=products[1].id)
    session.add(cart_product_1)
    session.add(cart_product_2)
    session.commit()

    return user, cart, products


def test_get_cart_fail(app, session):
    # If a cart does not exist for a user, then a GET request should raise a 404 NOT found error
    response = app.get('/cart')
    assert response.status_code == 404
    assert from_json(response.data) == {'error': 'Cart not found for user 1'}


def test_get_cart_success(app, session):
    # If a cart exists for a user, then a GET request should return a json response
    # of the Cart ID, and the related products in the cart
    user, cart, products = setup_data(app, session)
    expected_products = [
        {'id': products[0].id, 'name': products[0].name},
        {'id': products[1].id, 'name': products[1].name}
    ]

    response = app.get('/cart')
    assert response.status_code == 200

    payload = from_json(response.get_data())

    assert 'id' in payload and payload['id'] == cart.id
    assert 'products' in payload and payload['products'] == expected_products


def test_put_initialize_and_store_products(app, session):
    # If a cart does not exist for a user, it should be created and products
    # added and a 201 returned.
    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 1}, {'id': 3}]}
    response = app.put('/cart', data=to_json(payload), headers=headers)
    assert response.status_code == 201

    cart_product_qs = session.query(db.CartProduct).values(db.CartProduct.product_id)
    assert sum(cart_product_qs, tuple()) == (1, 3)


def test_put_store_products_on_existing_cart(app, session):
    # When a user has an existing cart, add products to the existing cart and
    # a 201 is returned.
    user, cart, products = setup_data(app, session)
    expected_products = [
        {'id': products[0].id, 'name': products[0].name},
        {'id': products[1].id, 'name': products[1].name},
        {'id': products[2].id, 'name': products[2].name}
    ]

    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 3}]}
    response = app.put('/cart', data=to_json(payload), headers=headers)

    assert response.status_code == 201
    assert from_json(response.data) == {'id': cart.id, 'products': expected_products}

    cart_product_qs = session.query(db.CartProduct).values(db.CartProduct.product_id)
    assert sum(cart_product_qs, tuple()) == (1, 2, 3)


def test_put_fails_when_all_given_product_ids_are_invalid(app, session):
    # If none of the supplied product ids, then the request should fail with a 404
    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 9999999}, {'id': 8888888888}]}
    response = app.put('/cart', data=to_json(payload), headers=headers)
    error_payload = from_json(response.get_data())

    assert response.status_code == 404
    assert error_payload == {'error': 'None of the products in the request exist in the db. Sorry!'}


def test_put_succeeds_when_at_least_one_product_id_is_valid(app, session):
    # If at least one product ID is valid, it'll not error out
    # and add the validated product to the cart.
    user, cart, products = setup_data(app, session)
    expected_products = [
        {'id': products[0].id, 'name': products[0].name},
        {'id': products[1].id, 'name': products[1].name},
        {'id': products[2].id, 'name': products[2].name}
    ]

    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 3}, {'id': 99999999}]}
    response = app.put('/cart', data=to_json(payload), headers=headers)

    assert response.status_code == 201
    assert from_json(response.data) == {'id': cart.id, 'products': expected_products}

    cart_product_qs = session.query(db.CartProduct).values(db.CartProduct.product_id)
    assert sum(cart_product_qs, tuple()) == (1, 2, 3)


def test_post_succeeds_with_valid_product_ids(app, session):
    # Test that POST succeeds when at least one product id is valid. Also tests
    # that the pre-existing items on the cart are removed.
    user, cart, products = setup_data(app, session)
    expected_products = [
        {'id': products[2].id, 'name': products[2].name},
        {'id': products[3].id, 'name': products[3].name},
    ]
    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 3}, {'id': 4}]}
    response = app.post('/cart', data=to_json(payload), headers=headers)

    response_json = from_json(response.data)
    assert response.status_code == 201
    assert response_json['id'] == session.query(db.Cart).filter(db.Cart.user_id).first().id
    assert response_json['products'] == expected_products

    cart_product_qs = session.query(db.CartProduct).values(db.CartProduct.product_id)
    assert sum(cart_product_qs, tuple()) == (3, 4)


def test_post_to_reset_cart(app, session):
    # Test that posting an empty product array, will reset the cart.
    user, cart, products = setup_data(app, session)
    headers = {'content-type': 'application/json'}
    payload = {'products': []}
    response = app.post('/cart', data=to_json(payload), headers=headers)

    response_json = from_json(response.data)
    assert response.status_code == 201
    assert response_json['id'] == cart.id
    assert response_json['products'] == []
    assert session.query(db.CartProduct).count() == 0


def test_post_fails_when_all_product_ids_are_invalid(app, session):
    # Test that when all supplied product ids are invalid, then an error is
    # raised.
    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 3999999}, {'id': 49999999}]}
    response = app.post('/cart', data=to_json(payload), headers=headers)

    assert response.status_code == 404
    assert from_json(response.data) == {'error': 'None of the products in the '
                                        'request exist in the db. Sorry!'}


def test_delete_existing_cart(app, session):
    # Test that cart is deleted.
    user, cart, products = setup_data(app, session)

    cart_id = cart.id
    response = app.delete('/cart')
    assert response.status_code == 204
    computed_cart_products = (session
                              .query(db.CartProduct)
                              .filter(db.CartProduct.cart_id==cart_id)
                              .all()
                              )
    # Assert that all CartProducts related to the specific cart were deleted on cascade.
    assert computed_cart_products == []


def test_create_order(app, session):
    # Create order from cart. If there are any products supplied in the post body,
    # expect them to be added to the final order
    user, cart, products = setup_data(app, session)
    headers = {'content-type': 'application/json'}
    payload = {'products': [{'id': 3}]}
    expected_products = [
        {'id': products[0].id, 'name': products[0].name},
        {'id': products[1].id, 'name': products[1].name},
        {'id': products[2].id, 'name': products[2].name}
    ]

    response = app.post('/cart/order', data=to_json(payload), headers=headers)
    assert response.status_code == 201
    assert from_json(response.data)['products'] == expected_products

    order_product_qs = session.query(db.OrderProduct).values(db.OrderProduct.product_id)
    assert sum(order_product_qs, tuple()) == (1, 2, 3)
